<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        //Count for Admin Dashboard
        $productsAll = Product::count();
        $usersAll = User::count();
        $ordersAll = Order::count();
        return view('admin.dashboard',compact('productsAll','usersAll','ordersAll'));



    }
}
