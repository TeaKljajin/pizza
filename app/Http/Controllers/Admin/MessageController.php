<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use Illuminate\Http\Request;
use App\Message;
class MessageController extends Controller
{
    public function index(){
        $messages = Message::paginate(10);
        return view('admin.messages.index',compact('messages'));
    }



    public function store(MessageRequest $request){

        //Save data to database
          Message::create([
            'name'=>$request->name,
            'email' => $request->email,
            'message' => $request->message,
        ]);

        //Session message
        session()->flash('msg','Message has been sent successfully');
        //Redirect to
        return redirect('/');
    }



    public function destroy($id){
        //Find a product
        $message = Message::findOrFail($id);
        //Delete message
        $message->delete();
        //Session message
        session()->flash('delete','Message has been deleted successfully');
        //Redirect to
        return redirect()->back();
    }


}
