<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\ShoppingCart;
use App\User;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    //Order Page all
    public function index(){
    $orders = Order::paginate(10);

    return view('admin.orders.index',compact('orders'));
    }

    //Status Pending
    public function pending($id){
        //Find the order
        $order = Order::findOrFail($id);
        ///Update the order
        $order->update([
            'status'=>0
        ]);
        //Session message
        session()->flash('msg','Order status has been updated');
        //Redirect to
        return redirect()->back();
    }

    //Status Confirm
    public function confirm($id){
        //Find the order
        $order = Order::findOrFail($id);
        ///Update the order
        $order->update([
            'status'=>1
        ]);
        //Session message
        session()->flash('msg','Order status has been updated');
        //Redirect to
        return redirect()->back();
    }

    //Order Details Page
    public function show($id){
        //Find the order
        $order = Order::findOrFail($id);
        //Redirect to
        return view('admin.orders.show',compact('order'));
    }


    //Checkout Page Delivery Store
    public function store(OrderRequest $request ){
        Order::create([
            //Delivery details
            'address' => $request->address,
            'phone' => $request->phone,
            'priceTotal' => $request->priceTotal,
            'taxTotal' => $request->taxTotal,
            'totalTotal' => $request->totalTotal,

            //Pending status
            'status' => $request->status,
            //User
            'user_id' => $request->user_id,

        ]);

        //Redirect to
        return redirect()->back();
    }


    //Checkout Page Make Order
    public function pay(Request $request ){

        //Find all users wishlist products
        $wishLists = ShoppingCart::where('user_id','Auth::user()->id')->get();
        foreach($wishLists as $wishList){
            $product = $wishList->product_id;
            //Find the last order on order table
            $order = Order::where('id', 'Auth::user()->id')->orderBy('id', 'DESC')->first();
            $quantity = ShoppingCart::where('id', 'Auth::user()->id')->find($request->input('quantity'));
            $price_sum = ShoppingCart::where('id', 'Auth::user()->id')->find($request->input('price_sum'));
            $tax_sum = ShoppingCart::where('id', 'Auth::user()->id')->find($request->input('tax_sum'));
            $tax = ShoppingCart::where('id', 'Auth::user()->id')->find($request->input('tax'));
            $order = Order::where('id', 'Auth::user()->id')->orderBy('id', 'DESC')->first();
            $user = User::where('id', 'Auth::user()->id');
            $status = 0;
            $order->products()->attach($product, $quantity, $price_sum, $tax_sum, $user, $status, $tax);


        }


        //Session message
        session()->flash('msg','You successfully make order ');
        //Redirect to
        return redirect('/');
    }





}
