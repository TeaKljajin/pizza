<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products = Product::paginate(5);
        return view('admin.products.index',compact('products'));
    }



    public function create(){
        $product = new Product();
        return view('admin.products.create',compact('product'));
    }


    public function store(ProductCreateRequest $request){
        //Upload image
        if($request->hasFile('image') ){
            $image = $request->image;
            $image->move('photos',$image->getClientOriginalName());
        }
        //Save data to database
        Product::create([
            'name'=>$request->name,
            'price'=>$request->price,
            'sku'=>$request->sku,
            'description'=>$request->description,
            'image'=>$request->image->getClientOriginalName()
        ]);
        //Session message
        session()->flash('msg','Product has been created successfully');
        //Redirect to
        return redirect('/admin/products');
    }



    public function edit($id){
        $product = Product::findOrFail($id);
        return view('admin.products.edit',compact('product'));
    }




    public function update(ProductUpdateRequest $request, $id){
        //Find a product
        $product = Product::findOrFail($id);
        //Upload Image
        if($request->hasFile('image')){
            if(file_exists(public_path('photos/').$product->image)){
                unlink(public_path('photos/').$product->image);
            }
            $image = $request->image;
            $image->move('photos',$image->getClientOriginalName());
            $product->image = $request->image->getClientOriginalName();

        }
        //Update data to database
        $product->update([
            'name'=>$request->name,
            'price'=>$request->price,
            'sku'=>$request->sku,
            'description'=>$request->description,
            'image'=> $product->image
        ]);
        //Session message
        session()->flash('msg','Product has been updated successfully');
        //Redirect to
        return redirect('/admin/products');

    }




    public function show($id){
        //Find the product
        $product = Product::findOrFail($id);
        return view('admin.products.show', compact('product'));
    }




    public function destroy($id){
        //Find a product
        $product = Product::findOrFail($id);
        //Unlink image
        if(file_exists(public_path('photos/').$product->image)){
            unlink(public_path('photos/').$product->image);
        }
        //Delete product
        $product->delete();
        //Session message
        session()->flash('delete','Product has been deleted successfully');
        //Redirect to
        return redirect()->back();
    }



}
