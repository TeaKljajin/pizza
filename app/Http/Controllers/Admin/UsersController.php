<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    //All Users in Admin Dashboard
    public function index(){
        $users = User::paginate(10);
        return view('admin.users.index',compact('users'));
    }



    //Register the user
        public function store(RegisterRequest $request){
            //Create new user
            $user = new User();
            //Save data to database
            $user = User::create([
                'name'=>$request->name,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'password' =>bcrypt($request->password) ,
                'role' => $request->role,
            ]);
            //Login user
            Auth::login($user);
            //Session message
            session()->flash('msg','You have been register in');
            //Redirect to
            return redirect('/');
        }


    //Users details in admin dashboard
    public function show($id){
        //Find the user
        $user = User::findOrFail($id);

        //Redirect to
        return view('admin.users.show',compact('user'));
    }


    //Buyer update personal information in Profile-edit page
    public function update(RegisterRequest $request, $id){
        //Find the user
        $user = User::findOrFail($id);

        if($request->get('password') != '' || $request->get('new_password') != '' || $request->get('password_confirmation') != '' ) {


            if (!(Hash::check($request->get('password'), Auth::user()->password))) {
                // The passwords not matches
                return redirect()->back()->with("old","Your current password does not matches with the password you provided. Please try again.");
            }
            if(strcmp($request->get('password'), $request->get('new_password')) == 0){
                //Current password and new password are same
                return redirect()->back()->with("new","New Password cannot be same as your current password. Please choose a different password.");
            }
            if(strcmp($request->get('new_password'), $request->get('password_confirmation')) !== 0){
                //Confirmation password and new password are not same
                return redirect()->back()->with("conf","Password Confirmation is not same as your new password. Please type the same password.");
            }
            $user->password = bcrypt($request['new_password']);
            //Save data to database
            $user->update([
                'name'=>$request->name,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'password' => $user->password,

            ]);
        }else{
            $user->update([
                'name'=>$request->name,
                'lastName' => $request->lastName,
                'email' => $request->email,
                'password' => Auth::user()->password,

            ]);


        }


        //Sessionmessage
        session()->flash('msg','Profile have been updated successfully');

        //Redirect to
        return redirect('/');

    }


}
