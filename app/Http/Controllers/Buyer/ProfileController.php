<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\ShoppingCart;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    //Main Profile Buyer Page
    public function index(){
        //Count Products where shoppingCart status == 0 to display in navbar
            $productsZero = ShoppingCart::where('status', 0)->where('user_id', Auth::user()->id)->count('id');


        $id = auth()->user()->id;
        $user = User::where('id',$id)->first();
        $orders = Order::where('user_id',$user->id)->first();
        return view('buyer.profile.index',compact('user','orders','productsZero'));
    }

    //Buyer change personal information
    public function edit($id){
        //Count Products where shoppingCart status == 0 to display in navbar
        $productsZero = ShoppingCart::where('status', 0)->where('user_id', Auth::user()->id)->count('id');
        $user = User::findOrFail($id);

        return view('buyer.profile.edit',compact('user','productsZero'));
    }

    //Buyer Order Details
    public function show($id){
        //Count Products where shoppingCart status == 0 to display in navbar
        $productsZero = ShoppingCart::where('status', 0)->where('user_id', Auth::user()->id)->count('id');
        $order = Order::findOrFail($id);
        return view('buyer.profile.show',compact('order','productsZero'));
    }


    //Buyer Product details
    public function details($id){
        //Count Products where shoppingCart status == 0 to display in navbar
        $productsZero = ShoppingCart::where('status', 0)->where('user_id', Auth::user()->id)->count('id');
        $product = Product::findOrFail($id);
        return view('buyer.profile.details',compact('product','productsZero'));
    }
}
