<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use App\User;
use App\Product;
use App\ShoppingCart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShoppingCartController extends Controller
{
    //ShoppingCart Page
    public function index()
    {
        //Find the user
        $user = User::where('id',Auth::user()->id)->first();

        //Find all products for that user to display in ShoppingCart
        $wishLists = ShoppingCart::orderBy('id','Desc')->where('user_id',$user->id)->get();

        //Count Products where shoppingCart status == 0
        $productsZero = ShoppingCart::where('status',0)->where('user_id',Auth::user()->id)->count('id');


        //Price_Sum all products where status is 0 => ShoppingCart
        $priceTotal = ShoppingCart::where('user_id', $user->id)->where('status',0)->sum('price_sum');
        $taxTotal = ShoppingCart::where('user_id', $user->id)->where('status',0)->sum('tax_sum');
        $totalTotal = (int) $priceTotal + (int) $taxTotal;


        return view('buyer.shoppingCart.index', compact('wishLists','priceTotal','taxTotal','totalTotal','productsZero'));
    }



    //Move Product From Public Front-end to ShoppingCart
    public function store(Request $request)
    {
        //Request data
        $product = $request->all();
        //Save data to database
        ShoppingCart::create($product);
        //Session message
        session()->flash('msg', 'Product has been moved to your ShoppingCart');
        //Redirect to
        return redirect()->back();

    }




    //Updating Product Quantity status = 0
    public function quantity(Request $request,$id){
        //Find the wishlist product
        $wishList = ShoppingCart::findOrFail($id);
        $price = (int) $request->price;
        $quantity = (int) $request->quantity;
        $price_sum = $quantity * $price ;
        $tax = (int) $request->tax;
        $tax_sum = (int) $quantity * $tax;

        //Update the shoppingCart quantity
        $wishList->update([
            'product_id' => $request->product_id,
            'user_id' => $request->user_id,
            'status' => $request->status,
            'tax'=> $request->tax,
            'tax_sum' => $tax_sum,
            'quantity' => $request->quantity,
            'price_sum' => $price_sum,
        ]);

        //Session message
        session()->flash('msg', 'Quantity has been updated');
        //Redirect to
        return redirect()->back();
    }



    //Removing product from ShoppingCart
    public function destroy($id)
    {
        //Find a product
        $product = ShoppingCart::findOrFail($id);
        $product->delete();
        //Session message
        session()->flash('delete', 'The product has been removed successfully from your ShoppingCart');
        //Redirect To
        return redirect()->back();
    }


    //Checkout Page
    public function proceed(){
        //Count Products where shoppingCart status == 0 to display in navbar
        $productsZero = ShoppingCart::where('status',0)->where('user_id',Auth::user()->id)->count('id');

        //Find the user
        $user = User::where('id',Auth::user()->id)->first();
        //Find all users wishlist
        $wishLists = ShoppingCart::where('user_id',$user->id)->get();

        //Sum all price products where status is 0 => Checkout
        $priceTotal = ShoppingCart::where('user_id', $user->id)->where('status',0)->sum('price_sum');
        $taxTotal = ShoppingCart::where('user_id', $user->id)->where('status',0)->sum('tax_sum');
        $totalTotal = (int) $priceTotal + (int) $taxTotal;

        return view('buyer.shoppingCart.checkout',compact('wishLists','user','priceTotal','taxTotal','totalTotal','productsZero','nizproducts'));
    }







}
