<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginReguest;

class LoginController extends Controller
{
    public function login(){
        return view('login');
    }
    public function register(){
        return view('register');
    }

    //Logout User from Dashboard
    public function logout(){
        Auth::logout();
        //Session message
        session()->flash('msg','You have been logout successfully');
        //Redirect to
        return redirect('/');
    }


    //Login User to Dashboard
    public function store(LoginReguest $request){

        //Request data from form
        $credentials = $request->only('email','password');

        //Check compatibility
        If(Auth::attempt($credentials)) {
            if(Auth::user()->role == 1){
                //Session message
                session()->flash('msg','You have been logged in');
                //Redirect to
                return redirect('/admin');
            }else{
                //Session message
                session()->flash('msg','You have been logged in');
                //Redirect to
                return redirect('/');
            }

        }else{
            return redirect('/login')->with([
                'errorMessage' => 'Wrong login details'
            ]);
        }

    }






}
