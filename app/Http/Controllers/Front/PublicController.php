<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\ShoppingCart;
use App\User;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function index(){

        //Count Products where shoppingCart status == 0 to display in navbar
        if(Auth::check()) {
            $productsZero = ShoppingCart::where('status', 0)->where('user_id', Auth::user()->id)->count('id');
        }

        $products = Product::all();
        return view('index',compact('products','productsZero'));
    }
}
