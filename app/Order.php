<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Order extends Model
{
    protected $fillable = [
        'user_id','address','phone','status','priceTotal','taxTotal','totalTotal'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function orderItems(){
        return $this->hasMany('App\OrderItem');
    }

    public function products(){
        return $this->belongsToMany('App\Product','order_items'); //pivot table
    }
}
