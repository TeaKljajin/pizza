<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['product_id','order_id','user_id','quantity','tax','price_sum','tax_sum','status'];


}
