<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class ShoppingCart extends Model
{
    protected $fillable = ['product_id','user_id','quantity','tax','tax_sum','status','price_sum'];

    public function product(){
        return $this->belongsTo('App\Product');
    }



}
