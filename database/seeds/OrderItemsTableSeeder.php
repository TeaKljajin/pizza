<?php

use Illuminate\Database\Seeder;
use App\OrderItem;
class OrderItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderItem::create([
            'id'=>1,
            'order_id'=>1,
            'product_id'=> 1,
            'user_id'=>1,
            'quantity'=> 2,
            'price_sum' =>800,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);


        OrderItem::create([
            'id'=>2,
            'order_id'=>1,
            'product_id'=> 1,
            'user_id'=>1,
            'quantity'=> 2,
            'price_sum' =>600,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);

        OrderItem::create([
            'id'=>3,
            'order_id'=>2,
            'product_id'=> 1,
            'user_id'=>1,
            'quantity'=> 2,
            'price_sum' =>500,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);

        OrderItem::create([
            'id'=>4,
            'order_id'=>2,
            'product_id'=> 2,
            'user_id'=>2,
            'quantity'=> 2,
            'price_sum' =>400,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
        OrderItem::create([
            'id'=>5,
            'order_id'=>2,
            'product_id'=> 2,
            'user_id'=>2,
            'quantity'=> 2,
            'price_sum' =>300,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
        OrderItem::create([
            'id'=>6,
            'order_id'=>2,
            'product_id'=> 1,
            'user_id'=>2,
            'quantity'=> 2,
            'price_sum' =>200,
            'tax_sum' => 30,
            'status' => 2,
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);

    }
}
