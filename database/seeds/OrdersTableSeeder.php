<?php

use Illuminate\Database\Seeder;
use App\Order;
class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'id'=>1,
            'user_id'=>1,
            'address'=> 'Belgrade',
            'phone'=>'3432423',
            'status'=>2,
            'priceTotal' => '1000',
            'taxTotal' => '40',
            'totalTotal' => '1040',
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
        Order::create([
            'id'=>2,
            'user_id'=>1,
            'address'=> 'Novi Sad',
            'phone'=>'34354',
            'status'=>2,
            'priceTotal' => '3000',
            'taxTotal' => '40',
            'totalTotal' => '4040',
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
        Order::create([
            'id'=>3,
            'user_id'=>2,
            'address'=> 'Belgrade',
            'phone'=>'3555444',
            'status'=>2,
            'priceTotal' => '1000',
            'taxTotal' => '40',
            'totalTotal' => '1040',
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
        Order::create([
            'id'=>4,
            'user_id'=>2,
            'address'=> 'Leskovac',
            'phone'=>'3499999',
            'status'=>2,
            'priceTotal' => '2000',
            'taxTotal' => '40',
            'totalTotal' => '2040',
            'created_at' => \Carbon\Carbon::parse('2000-01-01'),
        ]);
    }
}
