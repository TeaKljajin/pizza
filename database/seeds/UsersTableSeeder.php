<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'=>1,
            'name'=>'Admin',
            'lastName'=> 'AdminLastName',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('secret'),
            'role' => '1'
        ]);
        User::create([
            'id'=>2,
            'name'=>'User',
            'lastName'=> 'UserLastName',
            'email'=>'user@gmail.com',
            'password'=>bcrypt('secret1'),
            'role' => '0'
        ]);
        User::create([
            'id'=>3,
            'name'=>'Usersadsa',
            'lastName'=> 'UserLastNameasdd',
            'email'=>'user1@gmail.com',
            'password'=>bcrypt('secret1123'),
            'role' => '0'
        ]);
        User::create([
            'id'=>4,
            'name'=>'Userert',
            'lastName'=> 'UserLastNameete',
            'email'=>'userert@gmail.com',
            'password'=>bcrypt('secret1er'),
            'role' => '0'
        ]);
        User::create([
            'id'=>5,
            'name'=>'Userdgd',
            'lastName'=> 'UserLastNamedfg',
            'email'=>'userdfg@gmail.com',
            'password'=>bcrypt('secret1dfg'),
            'role' => '0'
        ]);
    }
}
