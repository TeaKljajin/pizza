@extends('admin.layouts.master')
@section('title') Admin Dashboard @endsection
@section('content')

    <div class="row" style="margin-left:5%;padding:3%;padding-bottom: 7%;">
        @include('admin.layouts.messages')
        <h1>Analyses</h1>
        <hr>
                <div class="container-fluid" style="margin-right:15%;">
                    <div class="row" >
                        <div class="col-lg-4 col-sm-6">
                            <div class="card" style="background-color: #01BFFF;">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="icon-big icon-success text-center">
                                                <i class="ti-archive"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="numbers">
                                                <p>Products</p>
                                                {{$productsAll}}
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('products.index')}}">
                                        <div class="footer">
                                            <hr/>
                                            <div class="stats" style="color:darkblue;">
                                                <i class="ti-panel"></i> Details
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card" style="background-color:#46BFBD;">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="icon-big icon-danger text-center">
                                                <i class="ti-shopping-cart-full"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="numbers">
                                                <p>Orders</p>
                                                {{$ordersAll}}
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('orders.index')}}">
                                        <div class="footer">
                                            <hr/>
                                            <div class="stats" style="color:darkgreen;">
                                                <i class="ti-panel"></i> Details
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="card" style="background-color:#949FB1;">
                                <div class="content">

                                    <div class="row">
                                        <div class="col-xs-5">
                                            <div class="icon-big icon-info text-center">
                                                <i class="ti-user"></i>
                                            </div>
                                        </div>
                                        <div class="col-xs-7">
                                            <div class="numbers">
                                                <p>Users</p>
                                                {{$usersAll}}
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{route('users.index')}}">
                                        <div class="footer">
                                            <hr/>
                                            <div class="stats" style="color:slategrey;">
                                                <i class="ti-panel"></i> Details
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <hr>
        <br>
        <div class="row" style="margin-right: 15%;" >
            <div class="col-md-6">
                <canvas  style="width:50px;" id="doughnutChart"></canvas>
            </div>
            <div class="col-md-6">
                <canvas id="barChart"></canvas>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>

        //doughnut
        var ctxD = document.getElementById("doughnutChart").getContext('2d');
        var myLineChart = new Chart(ctxD, {
            type: 'doughnut',
            data: {
                labels: ["Products", "Orders", "Users"],
                datasets: [{
                    data: [{{$productsAll}},{{$ordersAll}},{{$usersAll}}],
                    backgroundColor: ["#01BFFF", "#46BFBD", "#949FB1"],
                    hoverBackgroundColor: ["#1E90FF", "#5AD3D1", "#A8B3C5"]
                }]
            },
            options: {
                responsive: true
            }
        });


        var ctxB = document.getElementById("barChart").getContext('2d');
        var myBarChart = new Chart(ctxB, {
            type: 'bar',
            data: {
                labels: ["Products", "Orders", "Users"],
                datasets: [{
                    label:'Products',
                    data: [{{$productsAll}},{{$ordersAll}},{{$usersAll}}],
                    backgroundColor: ["#01BFFF", "#46BFBD", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#1E90FF", "#5AD3D1", "#A8B3C5"],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',

                    ],
                    borderWidth: 1

                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });


    </script>





@endsection
