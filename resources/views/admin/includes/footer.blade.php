<footer class="footer" style="background-color: #636b6f;">
    <div class="container-fluid">

        <div class="copyright pull-right" style="color:white;">
            All Rights Reserved by <a href="" style="font-size: 18px;font-weight: bolder;color:mediumspringgreen;">Tea Kljajin</a>
            &copy;
            <script>document.write(new Date().getFullYear())</script>

        </div>
    </div>
</footer>
