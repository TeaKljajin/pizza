<nav class="navbar navbar-default" style="background-color:#696969;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar bar1"></span>
                <span class="icon-bar bar2"></span>
                <span class="icon-bar bar3"></span>
            </button>
            <a class="navbar-brand" href="#" style="color:lightseagreen">Dashboard</a>
            <a class="navbar-brand" href="{{route('public.index')}}" style="color:white;"> Home</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:lightseagreen">
                        <i class="ti-settings"></i>
                        <p style="font-weight: bolder;"><i class="fa fa-user"></i> {{Auth::user()->name}}</p>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="{{route('logout')}}">Logout</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
</nav>
