<div class="sidebar" data-background-color="dark" data-active-color="danger">

    <div class="sidebar-wrapper" style="background-color: #696969;">
        <div class="logo">
            <a href="" class="simple-text" style="color:white;font-weight: bolder;font-size: 20px;font-family:Arial;">
                PizzaShop Admin
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{route('admin.dashboard')}}" style="color:white">
                    <i class="ti-panel"></i>
                    <p style="font-size: 18px;text-transform: capitalize;">Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{route('products.index')}}" style="color:white;">
                    <i class="ti-archive"></i>
                    <p style="font-size: 18px;text-transform: capitalize;">Product</p>
                </a>
            </li>

            <li>
                <a href="{{route('orders.index')}}" style="color:white;">
                    <i class="ti-calendar"></i>
                    <p style="font-size: 18px;text-transform: capitalize;">Orders</p>
                </a>
            </li>
            <li>
                <a href="{{route('users.index')}}" style="color:white">
                    <i class="fa fa-users"></i>
                    <p style="font-size: 18px;text-transform: capitalize;">Users</p>
                </a>
            </li>
            <li>
                <a href="{{route('message.index')}}" style="color:white">
                    <i class="fa fa-pencil"></i>
                    <p style="font-size: 18px;text-transform: capitalize;">Messages</p>
                </a>
            </li>
        </ul>
    </div>



</div>
