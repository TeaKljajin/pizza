@include('admin.includes.header')
<body>

<div class="wrapper">
    @include('admin.includes.sidebar')

    <div class="main-panel" style="background-color: white;">
        @include('admin.includes.navbar')

        @yield('content')


    </div>

</div>

@include('admin.includes.footer')

@yield('script')
@include('admin.includes.scripts')







