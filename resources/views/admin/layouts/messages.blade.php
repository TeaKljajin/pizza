
@if(session()->has('msg'))
    <div class="alert alert-success alert-dismissible fade show mt-3" role="alert">
        <strong>{{session()->get('msg')}}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif


@if(session()->has('delete'))
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        <strong>{{session()->get('delete')}}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
