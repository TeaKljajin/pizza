@extends('admin.layouts.master')
@section('title') Messages @endsection
@section('content')

    <div class="row" style="margin-left:2%;margin-right: 2%;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <h3 class="text-center" style="font-weight: bolder">Messages </h3>
                </div>
                <div class="col-md-3">

                </div>
            </div>
            @include('admin.layouts.messages')
            <div class="content table-responsive table-full-width">

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="font-weight: bolder;">Id</th>
                        <th style="font-weight: bolder;">Name</th>
                        <th style="font-weight: bolder;">Email</th>
                        <th style="font-weight: bolder;">Message</th>
                        <th style="font-weight: bolder;">Created at</th>
                        <th style="font-weight: bolder;">Delete</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($messages as $message)
                        <tr>
                            <td>{{$message->id}}</td>
                            <td>{{$message->name}}</td>
                            <td>{{$message->email}}</td>
                            <td>{{$message->message}}</td>
                            <td>{{$message->created_at}}</td>
                            <th> {!! Form::open(['route'=>['message.destroy',$message->id],'method'=>'post']) !!}
                                {{Form::submit('Delete',['type'=>'submit','class'=>'btn btn-danger btn-sm','onclick'=>'return confirm("Are you sure you want to delete this message?")'])}}
                                {!! Form::close() !!}</th>


                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <!--------Pagination------>
        <div class="row">
            <div class="col-sm-6 col-lg-offset-5">
                {{$messages->render()}}
            </div>
        </div>
        <!--------End Pagination------>


    </div>


@endsection


