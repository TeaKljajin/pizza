@extends('admin.layouts.master')
@section('title') Orders @endsection
@section('content')

    @if(!Auth::check())
        <script>window.location='/'</script>
    @endif

    <div class="row " style="margin-left:2%;margin-right: 2%;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <h3 class="text-center" style="font-weight: bolder">Orders </h3>
                </div>
                <div class="col-md-3">
                </div>
            </div>

            <div class="content table-responsive table-full-width">
                @include('admin.layouts.messages')
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="font-weight: bolder;">Id</th>
                        <th style="font-weight: bolder;">User Id</th>
                        <th style="font-weight: bolder;">Address</th>
                        <th style="font-weight: bolder;">Phone</th>
                        <th style="font-weight: bolder;">PriceTotal</th>
                        <th style="font-weight: bolder;">TaxTotal</th>
                        <th style="font-weight: bolder;">Total</th>
                        <th style="font-weight: bolder;">Created at</th>
                        <th style="font-weight: bolder;">Status</th>
                        <th style="font-weight: bolder;">Action</th>
                        <th style="font-weight: bolder;">Details</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>{{$order->user_id}}</td>
                            <td>{{$order->address}}</td>
                            <td>{{$order->phone}}</td>
                            <td>{{$order->priceTotal}}</td>
                            <td>{{$order->taxTotal}}</td>
                            <td>{{$order->totalTotal}}</td>
                            <td>{{$order->created_at->diffForHumans()}}</td>
                            <th>
                                @if($order->status)
                                    <span class="label label-success btn-sm">Confirmed</span>
                                @else
                                    <span class="label label-warning btn-sm">Pending</span>
                                @endif
                            </th>
                            <th>
                                @if($order->status)
                                    {{link_to_route('order.pending','Pending',$order->id,['class'=>'btn btn-warning btn-sm'])}}
                                @else
                                    {{link_to_route('order.confirm','Confirm',$order->id,['class'=>'btn btn-success btn-sm'])}}
                                @endif
                            </th>
                            <th>   {{link_to_route('orders.show','Details',$order->id,['class'=>'btn btn-info btn-sm ti-list'])}}</th>



                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <!--------Pagination------>
        <div class="row">
            <div class="col-sm-6 col-lg-offset-5">
                {{$orders->render()}}
            </div>
        </div>
        <!--------End Pagination------>
    </div>


@endsection


