
@extends('admin.layouts.master')
@section('title') Orders Details @endsection
@section('content')

    @if(!Auth::check())
        <script>window.location='/'</script>
    @endif

    <div class="row" style="margin-left:2%;margin-right: 3%;margin-bottom: 10%;">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="font-weight: bolder">Orders Details</h4>
                    <br>
                    <a href="{{route('orders.index')}}" class="btn btn-primary active btn-sm" style="background-color: cornflowerblue;" role="button" aria-pressed="true">Go Back</a>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <div class="content table-responsive table-full-width">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th>User Id</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>PriceTotal</th>
                                    <th>TaxTotal</th>
                                    <th>Total</th>
                                    <th>Created at</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{$order->id}}</td>
                                    <td>{{$order->user_id}}</td>
                                    <th>{{$order->address}}</th>
                                    <th>{{$order->phone}}</th>
                                    <th>{{$order->priceTotal}}</th>
                                    <th>{{$order->taxTotal}}</th>
                                    <th>{{$order->totalTotal}}</th>

                                    <td>{{$order->created_at->diffForHumans()}}</td>



                                    <th>
                                        @if($order->status)
                                            <span class="label label-success btn-sm">Confirmed</span>
                                        @else
                                            <span class="label label-warning btn-sm">Pending</span>
                                        @endif
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="font-weight: bolder">User Details</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>

                                <tr>
                                    <th>User Id</th>
                                    <td>{{$order->user_id}}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{$order->user->name}}</td>
                                </tr>
                                <tr>
                                    <th>Last Name</th>
                                    <td>{{$order->user->lastName}}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{$order->user->email}}</td>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="header">
                    <h4 class="title" style="font-weight: bolder">Product Details</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Product Id</th>
                            <th>Name</th>
                            <th>Sku</th>
                            <th>Quantity</th>
                            <th>Price per 1</th>
                            <th>PriceSum</th>
                            <th>Image</th>
                        </tr>
                        </thead>
                        <tbody>
                        <td>
                            @foreach($order->products as $product)
                                {{$product->id}}
                                @if($order->products->count() > 1)
                                    <hr>
                                @endif
                            @endforeach
                        </td>

                        <td>
                            @foreach($order->products as $product)
                                {{$product->name}}
                                @if($order->products->count() > 1)
                                    <hr>
                                @endif

                            @endforeach
                        </td>
                        <td>
                            @foreach($order->products as $product)
                                {{$product->sku}}
                                @if($order->products->count() > 1)
                                    <hr>
                                @endif

                            @endforeach
                        </td>
                        <td>
                            @foreach($order->orderItems as $item)
                                {{$item->quantity}}

                                @if($order->orderItems->count() > 1)
                                    <hr>
                                @endif

                            @endforeach
                        </td>

                        <td>
                            @foreach($order->products as $product)
                                {{$product->price}}
                                @if($order->products->count() > 1)
                                    <hr>
                                @endif

                            @endforeach
                        </td>
                        <td>
                            @foreach($order->orderItems as $item)
                                {{$item->price_sum}}

                                @if($order->orderItems->count() > 1)
                                    <hr>
                                @endif
                            @endforeach
                        </td>

                        <td>
                            @foreach($order->products as $item)
                                <img src="{{asset('photos/'.$item->image)}}" alt="Slika" style="width:50px;" class="img-thumbnail">
                                @if($order->products->count() > 1)
                                    <br>
                                    <br>

                                @endif

                            @endforeach
                        </td>

                        </tbody>



                    </table>
                </div>
            </div>

        </div>
    </div>


@endsection
