@extends('admin.layouts.master')
@section('title') Products Create @endsection
@section('content')

    <div class="row" style="margin-left:2%;margin-right: 2%;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('products.index')}}" class="btn btn-primary btn-sm" style="margin-top:5%;background-color:#5bc0de;" role="button" aria-pressed="true">Go Back</a>
                </div>
                <div class="col-md-3">
                    <h3 class="text-center" style="font-weight: bolder">Products Edit</h3>
                </div>
                <div class="col-md-3">

                </div>
            </div>


            <div class="row">
                <div class="col-md-9">
                    {!! Form::open(['url'=>['/admin/products',$product->id],'method'=>'put','files'=>true]) !!}
                    <div class="form-group">
                        {{Form::label('name','Product Name:')}}
                        {{Form::text('name',$product->name,['class'=>'form-control border-input','placeholder'=>'Macboook pro'])}}
                        @error('name')<p style="color:red;">{{$message}}</p> @enderror
                    </div>


                    <div class="form-group">
                        {{Form::label('price','Product Price:')}}
                        {{Form::text('price',$product->price,['class'=>'form-control border-input','placeholder'=>'$2500'])}}
                        @error('price') <p style="color:red;">{{$message}} </p> @enderror
                    </div>
                    <div class="form-group">
                        {{Form::label('sku','Sku:')}}
                        {{Form::text('sku',$product->sku,['class'=>'form-control border-input','placeholder'=>'BK-1234'])}}
                        @error('sku') <p style="color:red;">{{$message}} </p> @enderror
                    </div>

                    <div class="form-group">
                        {{Form::label('description','Product Description:')}}
                        {{Form::textarea('description',$product->description,['class'=>'form-control border-input','placeholder'=>'Product Description'])}}
                        @error('description') <p style="color:red;">{{$message}} </p> @enderror
                    </div>
                    <div class="form-group">
                        {{Form::label('image','Product Image:')}}
                        {{Form::file('image',['class'=>'form-control border-input'])}}
                        @error('image') <p style="color:red;"> {{$message}} </p> @enderror
                        <div id="thumb-output"></div>
                    </div>

                    <div class="form-group">
                        {{Form::submit('Update Product',['class'=>'btn btn-primary btn-submit btn-sm','style'=>'background-color:lightseagreen;color:white'])}}

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>




        </div>
    </div>
    </div>


@endsection



