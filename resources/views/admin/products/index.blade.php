@extends('admin.layouts.master')
@section('title') Products @endsection
@section('content')

    <div class="row" style="margin-left:2%;margin-right: 2%;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <a href="{{route('products.create')}}" class="btn btn-primary btn-sm active " style="margin-top:5%;background-color: lightseagreen;" role="button" aria-pressed="true">Create New Product</a>
                </div>
                <div class="col-md-3">
                    <h3 class="text-center" style="font-weight: bolder">Products </h3>
                </div>
                <div class="col-md-3">

                </div>
            </div>
            @include('admin.layouts.messages')
                <div class="content table-responsive table-full-width">

                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="font-weight: bolder;">Id</th>
                            <th style="font-weight: bolder;">Name</th>
                            <th style="font-weight: bolder;">Price</th>
                            <th style="font-weight: bolder;">Sku</th>
                            <th style="font-weight: bolder;">Description</th>
                            <th style="font-weight: bolder;">Image</th>
                            <th style="font-weight: bolder;">Edit</th>
                            <th style="font-weight: bolder;">Details</th>
                            <th style="font-weight: bolder;">Delete</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->price}}</td>
                                <td>{{$product->sku}}</td>
                                <td>{{Illuminate\Support\Str::limit($product->description,25)}}</td>
                                <td><img src="{{asset('photos/'.$product->image)}}" alt="Slika" style="width:100px;" class="img-thumbnail"></td>
                                <th>{{link_to_route('products.edit','Edit',$product->id,['class'=>'btn btn-info btn-sm ti-pencil'])}}    </th>

                                 <th>   {{link_to_route('products.show','Details',$product->id,['class'=>'btn btn-info btn-sm ti-list'])}}</th>
                                  <th> {!! Form::open(['route'=>['products.destroy',$product->id],'method'=>'delete']) !!}
                                    {{Form::submit('Delete',['type'=>'submit','class'=>'btn btn-danger btn-sm','onclick'=>'return confirm("Are you sure you want to delete this product?")'])}}
                                    {!! Form::close() !!}</th>


                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
        </div>
        <!--------Pagination------>
        <div class="row">
            <div class="col-sm-6 col-lg-offset-5">
                {{$products->render()}}
            </div>
        </div>
        <!--------End Pagination------>


    </div>


@endsection

