@extends('admin.layouts.master')
@section('title') Products Details @endsection
@section('content')

    <div class="row" style="margin-left:2%;margin-right: 2%;margin-bottom: 17%;">
        <div class="col-md-12">
            <div class="card">
                <div class="header">


                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a href="{{route('products.create')}}" class="btn btn-primary btn-sm active"  style="background-color: lightseagreen;" role="button" aria-pressed="true">Create New Product</a>
                        <a href="{{route('products.index')}}" class="btn btn-primary btn-sm active"  style="background-color: lightskyblue;"  role="button" aria-pressed="true">Go Back</a>
                        <br>
                        <br>
                        <img src="{{asset('photos/'.$product->image)}}" alt="{{asset('photos/'.$product->name)}}" style="width:400px;height:300px;margin-top:3%; " class="img-thumbnail img-responsive">
                    </div>
                    <div class="col-md-6">
                        <div class="content table-responsive table-full-width">
                            <h4 class="title text-center" style="font-weight: bolder">Products Details</h4>
                            <br>
                            <table class="table table-bordered">
                                <tbody>

                                <tr>
                                    <th>Id</th>
                                    <td>{{$product->id}}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{$product->name}}</td>
                                </tr>
                                <tr>
                                    <th>Price</th>
                                    <td>{{$product->price}}</td>
                                </tr>
                                <tr>
                                    <th>Sku</th>
                                    <td>{{$product->sku}}</td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td>{{$product->description}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
                <br>

            </div>
        </div>
    </div>


@endsection


