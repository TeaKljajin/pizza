@extends('admin.layouts.master')
@section('title') Users @endsection
@section('content')

    @if(!Auth::check())
        <script>window.location='/'</script>
    @endif

    <div class="row" style="margin-left:2%;margin-right: 2%;padding-bottom: 30%;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-3">
                    <h3 class="text-center" style="font-weight: bolder">Users </h3>
                </div>
                <div class="col-md-3">
                </div>
            </div>

            <div class="content table-responsive table-full-width">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th style="font-weight: bolder;">Id</th>
                        <th style="font-weight: bolder;">Name</th>
                        <th style="font-weight: bolder;">Last Name</th>
                        <th style="font-weight: bolder;">E-mail</th>
                        <th style="font-weight: bolder;">Created at</th>
                        <th style="font-weight: bolder;">Details</th>


                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->lastName}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->created_at->diffForHumans()}}</td>
                            <th>   {{link_to_route('users.show','Details',$user->id,['class'=>'btn btn-info btn-sm ti-list'])}}</th>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <!--------Pagination------>
        <div class="row">
            <div class="col-sm-6 col-lg-offset-5">
                {{$users->render()}}
            </div>
        </div>
        <!--------End Pagination------>
    </div>


@endsection

