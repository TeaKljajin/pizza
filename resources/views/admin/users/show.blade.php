@extends('admin.layouts.master')
@section('title') Users Details @endsection
@section('content')

    <div class="row" style="margin-left:2%;margin-right: 3%;margin-bottom: 20%;">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">User Order Details</h4>
                    <p class="category">User order details</p>
                    <br>
                    <a href="{{route('users.index')}}" class="btn btn-primary btn-sm active" style="background-color: lightskyblue;" role="button" aria-pressed="true">Go Back</a>
                    <hr>
                </div>

                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>PriceTotal</th>
                            <th>TaxTotal</th>
                            <th>Total</th>
                            <th>Order Date</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->address}}</td>
                                <td>{{$order->phone}}</td>
                                <td>{{$order->priceTotal}}</td>
                                <td>{{$order->taxTotal}}</td>
                                <td>{{$order->totalTotal}}</td>
                                <td>{{$order->created_at->diffForHumans()}}</td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection


