@extends('front.layouts.master')
@section('title') Buyer Edit @endsection
@section('content')
    <div class="container">
        <div class="container" style="padding-bottom:30%;background-color: white;padding-top:5%;">
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center"><strong>Edit Profile</strong></h3>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <a href="{{route('buyer.index',$user->id)}}" class="btn btn-info btn-sm mb-3" role="button" style="margin-left:2%;" aria-pressed="true">Go Back</a>

                            {!! Form::model($user,['method'=>'post','action'=>['Admin\UsersController@update',$user->id]]) !!}
                            @csrf
                            {{Form::hidden('role',0)}}
                            <div class="form-group col-md-12">
                                {{Form::label('name','Name:')}}
                                {{Form::text('name',$user->name,['class'=>'form-control border-input','autocomplete'=>'off','id'=>'name','placeholder'=>'Name'])}}
                                @error('name') <div class="alert alert-danger">{{$message}}</div> @enderror
                            </div>
                            <div class="form-group col-md-12">
                                {{Form::label('lastName','Last Name:')}}
                                {{Form::text('lastName',$user->lastName,['class'=>'form-control border-input','autocomplete'=>'off','id'=>'lastName','placeholder'=>'Last name'])}}
                                @error('lastName') <div class="alert alert-danger">{{$message}}</div> @enderror
                            </div>
                            <div class="form-group col-md-12">
                                {{Form::label('email','Email:')}}
                                {{Form::email('email',$user->email,['class'=>'form-control border-input','autocomplete'=>'off','id'=>'email','placeholder'=>'Email'])}}
                                @error('email') <div class="alert alert-danger">{{$message}}</div> @enderror
                            </div>
                            <div class="row" style="margin-left:3px;margin-right:2px;">
                                <div class="form-group col-4">
                                    {{Form::label('password','Password:')}}
                                    {{Form::password('password',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'password','placeholder'=>'Old password'])}}
                                   @if(session()->has('old'))
                                       <div class="alert alert-danger">
                                           {{session()->get('old')}}
                                       </div>
                                       @endif

                                </div>
                                <div class="form-group col-4">
                                    {{Form::label('new_password','New Password:')}}
                                    {{Form::password('new_password',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'new_password','placeholder'=>'New password'])}}
                                    @if(session()->has('new'))
                                        <div class="alert alert-danger">
                                            {{session()->get('new')}}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col-4">
                                    {{Form::label('password_confirmation','Confirm Password:')}}
                                    {{Form::password('password_confirmation',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'password_confirmation','placeholder'=>'Confirm password'])}}
                                    @if(session()->has('conf'))
                                        <div class="alert alert-danger">
                                            {{session()->get('conf')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                {{Form::submit('Submit',['class'=>'btn btn-primary btn-sm','style'=>'margin-left:15px;'])}}
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


