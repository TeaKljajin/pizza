@extends('front.layouts.master')
@section('title') Profile @endsection
@section('content')
    <div class="container" style="background-color:white;padding:1%;padding-bottom:15%;" >
        <h1 class="text-center mt-3" style="color:cornflowerblue;">Welcome To Your Profile Page</h1>
        <h3 style="color:cornflowerblue">Profile</h3>
        {{link_to_route('buyer.edit','Edit Profile',$user->id,['class'=>'btn btn-info btn-sm'])}}
        <div class="row mt-3" style="padding:1%;">
            <div class="col-lg-12" style="background-image:url('/photos/wine.png');background-repeat: no-repeat;height:400px;width:100%;  background-position: right; ">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th colspan="2">User Details  </th>


                <br>
                 <hr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <td style="color:cornflowerblue;font-weight: bolder;font-size: 20px;">{{$user->name}}</td>
                    </tr>
                    <tr>
                        <th>Last Name</th>
                        <td style="color:cornflowerblue;font-weight: bolder;font-size: 20px;">{{$user->lastName}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td style="color:cornflowerblue;font-weight: bolder;font-size: 20px;">{{$user->email}}</td>
                    </tr>
                    <tr>
                        <th>Created at</th>
                        <td style="color:cornflowerblue;font-weight: bolder;font-size: 20px;">{{$user->created_at->diffForHumans()}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>


        </div>


        <h4 class="title mt-2 mb-3" style="color:cornflowerblue">Orders</h4>

        <div class="content table-responsive table-full-width">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th style="font-weight: bolder;">Id</th>
                    <th style="font-weight: bolder;">Address</th>
                    <th style="font-weight: bolder;">Phone</th>
                    <th style="font-weight: bolder;">PriceTotal</th>
                    <th style="font-weight: bolder;">TaxTotal</th>
                    <th style="font-weight: bolder;">Total</th>
                    <th style="font-weight: bolder;">Created at</th>
                    <th style="font-weight: bolder;">Status</th>
                    <th>Created at</th>
                    <th style="font-weight: bolder;">Details</th>

                </tr>
                </thead>
                <tbody>
                @foreach($user->orders as $order)
                    <tr>

                        <td>{{$order->id}}</td>
                        <td>{{$order->address}}</td>
                        <td>{{$order->phone}}</td>
                        <td>{{$order->priceTotal}}</td>
                        <td>{{$order->taxTotal}}</td>
                        <td>{{$order->totalTotal}}</td>
                        <td>{{$order->created_at->diffForHumans()}}</td>
                        <th>
                            @if($order->status)
                                <span class="label label-success btn-sm">Confirmed</span>
                            @else
                                <span class="label label-warning btn-sm">Pending</span>
                            @endif
                        </th>

                        <td>{{$order->created_at->diffForHumans()}}</td>

                        <th>

                            {{link_to_route('buyer.show','Details',$order->id,['class'=>'btn btn-info btn-sm'])}}
                        </th>

                    </tr>

                @endforeach

                </tbody>
            </table>

        </div>
    </div>


@endsection

