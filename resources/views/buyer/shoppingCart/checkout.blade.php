@extends('front.layouts.master')
@section('title') Checkout @endsection
@section('content')

    <div class="container " style="background-color:white;padding:3%;padding-bottom: 14%;">

        <h2 class="mt-1"><i class="fa  fa-credit-card-alt"></i> Checkout</h2>
        <hr>
        <!--Start row---->
        <div class="row">
            <div class="col-md-6">

                <!-----Delivery Details----->

                {!! Form::open(['method'=>'post','action'=>'Admin\OrdersController@store']) !!}
                @csrf
                {{Form::hidden('user_id',Auth::user()->id)}}
                {{Form::hidden('status',0)}}
                {{Form::hidden('priceTotal',$priceTotal)}}
                {{Form::hidden('taxTotal',$taxTotal)}}
                {{Form::hidden('totalTotal',$totalTotal)}}



                    <div class="form-group">
                        <h4>Delivery Details</h4>
                        {{Form::label('address','Address:')}}
                        {{Form::text('address','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'address','placeholder'=>'Address'])}}
                        @error('address') <div class="alert alert-danger">{{$message}}</div> @enderror
                    </div>
                    <div class="form-group">
                        {{Form::label('phone','Phone Number:')}}
                        {{Form::number('phone','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'phone','placeholder'=>'Phone'])}}
                        @error('phone') <div class="alert alert-danger">{{$message}}</div> @enderror
                    </div>

                    <div class="form-group">
                        {{Form::submit('Submit',['class'=>'btn btn-primary btn-info btn-sm'])}}
                    </div>
                {!! Form::close() !!}
            <!-----End Delivery Details----->
                </div>



            <!-----Price Details-------->
            <div class="col-md-6 ">
                <table class="table your-order-table table-bordered text-center mt-5">
                    <tr>
                        <th colspan="2">Price Details</th>
                    </tr>
                    <tr>
                        <td>PriceTotal</td>
                        <td>${{$priceTotal  }}</td>
                    </tr>
                    <tr>
                        <td>TaxTotal</td>
                        <td>${{$taxTotal}}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <th>${{$totalTotal}}</th>
                    </tr>

                </table>

                <!-----End Price Details-------->

            </div>
        </div>



        <div class="row">
                   <div class="col-md-6">
                       <!-------Card Details--------->
                       {!! Form::open(['method'=>'post','action'=>'Admin\OrdersController@pay']) !!}
                       @csrf
                       @foreach($wishLists as $wishList)
                       {{Form::hidden('status',0)}}
                       {{Form::hidden('quantity',$wishList->quantity)}}
                       {{Form::hidden('tax_sum',$wishList->tax_sum)}}
                       {{Form::hidden('price_sum',$wishList->price_sum)}}
                       {{Form::hidden('tax',$wishList->tax)}}
                       @endforeach


                       <h4>Card Details</h4>
                       <div class="form-group">
                           {{Form::label('card_name','Card Name:')}}
                           {{Form::number('card_name','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'card_name','placeholder'=>'Card name'])}}
                           @error('card_name') <div class="alert alert-danger">{{$message}}</div> @enderror
                       </div>
                       <div class="form-group">
                           {{Form::label('credit_card_number','Credit Card Number:')}}
                           {{Form::number('credit_card_number','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'credit_card_number','placeholder'=>'Credit card number'])}}
                           @error('credit_card_number') <div class="alert alert-danger">{{$message}}</div> @enderror
                       </div>
                       <div class="form-group">
                           {{Form::label('expiration_month','Expiration Month:')}}
                           {{Form::number('expiration_month','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'expiration_month','placeholder'=>'Expiration month'])}}
                           @error('expiration_month') <div class="alert alert-danger">{{$message}}</div> @enderror
                       </div>
                       <div class="form-group">
                           {{Form::label('expiration_year','Expiration Year:')}}
                           {{Form::number('expiration_year','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'expiration_year','placeholder'=>'Expiration year'])}}
                           @error('expiration_year') <div class="alert alert-danger">{{$message}}</div> @enderror
                       </div>
                       <div class="form-group">
                           {{Form::label('card_cvc','Security code:')}}
                           {{Form::text('card_cvc','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'card_cvc','placeholder'=>'Security code'])}}
                           @error('card_cvc') <div class="alert alert-danger">{{$message}}</div> @enderror
                       </div>
                       <div class="form-group">
                           {{Form::submit('Buy now',['class'=>'btn btn-primary btn-info btn-sm'])}}
                       </div>
                       {!! Form::close() !!}

                   </div>
                   <!-------Card Details--------->


                   <div class="col-md-6">
                       <!---- Product Details------->

                       <h4>Products Order Details</h4>
                       <br>
                       <table class="table your-order-table">
                           <thead>
                           <tr><th>Id</th>
                               <th>Image</th>
                               <th>Name</th>
                               <th>PriceSum </th>
                               <th>Qty</th>
                           </tr>
                           </thead>
                           <tbody>

                           @foreach($wishLists as $wishList)

                               @if($wishList->status == 0)

                                   <tr><td>{{$wishList->product_id}}</td>
                                       <td><img src="{{asset('photos/'.$wishList->product->image)}}" style="width: 5em;height:70px;"></td>
                                       <td>{{Illuminate\Support\Str::limit($wishList->product->name, 10)}}</td>
                                       <td>{{$wishList->price_sum}}</td>
                                       <td>{{$wishList->quantity}}</td>
                                   </tr>
                               @endif
                           @endforeach
                           </tbody>
                       </table>
                       <!----End ProductDetails------->
                   </div>
               </div>

            </div>

        <!--End row---->

@endsection


