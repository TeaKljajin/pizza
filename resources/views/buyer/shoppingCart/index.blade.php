@extends('front.layouts.master')
@section('title') ShoppingCart @endsection
@section('content')

    <!-- Page Content -->
    <div class="container" style="background-color:white;padding:2%;padding-bottom: 22%;">

        <h2 class="mt-3 text-center"><i class="fa fa-shopping-cart"></i> Shooping Cart</h2>
        <hr>
        <a href="{{route('public.index')}}" class="btn btn-info btn-sm mb-3" role="button" aria-pressed="true">Go Back</a>

         @include('buyer.includes.messages')

        <h4 class="mt-1"> {{$productsZero}} items(s) in Shopping Cart</h4>

        <div class="cart-items">
            <div class="row">
                <div class="col-md-12">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Price per 1</th>
                            <th>Tax</th>
                            <th>Quantity</th>
                            <th>PriceSum</th>
                            <th>TaxSum</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($wishLists->count()>0)
                        @foreach($wishLists as $wishList)
                            @if($wishList->status == 0)
                                <tr>
                                    <td><img src="{{asset('photos/'.$wishList->product->image)}}" style="width: 5em;height:70px;"></td>
                                    <td>
                                        <strong>{{$wishList->product->name}}</strong><br>  {{Illuminate\Support\Str::limit($wishList->product->description,35)}}
                                    </td>
                                    <td>$ {{$wishList->product->price}}</td>
                                    <td>${{$wishList->tax}}</td>
                                    <td>

                                        <!------Change the quantity------>
                                    {!! Form::open(['method'=>'post','action'=>['Buyer\ShoppingCartController@quantity',$wishList->id]]) !!}
                                    @csrf
                                    {{Form::hidden('product_id',$wishList->product->id)}}
                                    {{Form::hidden('user_id',Auth::user()->id)}}
                                    {{Form::hidden('status',0)}}
                                    {{Form::hidden('priceTotal',0)}}
                                    {{Form::hidden('tax',2)}}
                                    {{Form::hidden('price',$wishList->product->price)}}
                                    {{Form::selectRange('quantity', 1, 50, ['placeholder'=> $wishList->quantity])}}
                                    {{Form::submit('Save',['class'=>'btn btn-success btn-info btn-sm'])}}

                                    {!! Form::close() !!}
                                    <!------End Change the quantity------>

                                    </td>
                                    <td>${{$wishList->price_sum}}</td>
                                    <td>${{$wishList->tax_sum}}</td>
                                    <td>
                                        <!--Removing item from ShoppingCart-->
                                        {!! Form::open(['method'=>'post','action'=>['Buyer\ShoppingCartController@destroy',$wishList->id]]) !!}
                                        @csrf
                                        {{Form::submit('Remove',['class'=>'btn btn-danger btn-info btn-sm','onclick'=>'return confirm("Are you sure you want to remove product from ShoppingCart?")'])}}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6 mt-1">
                <table class="table your-order-table table-bordered text-center">
                    <tr>
                        <th colspan="2">Price Details</th>
                    </tr>
                    <tr>
                        <td>PriceTotal</td>
                        <td>${{$priceTotal  }}</td>
                    </tr>
                    <tr>
                        <td>TaxTotal</td>
                        <td>${{$taxTotal}}</td>
                    </tr>
                    <tr>
                        <th>Total</th>
                        <th>${{$totalTotal}}</th>
                    </tr>
                </table>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-info text-center" href="{{route('public.index')}}" role="button">Continue Shopping</a>


{{--                    <!------Remove all products from ShopppingCart and go to Checkout => Status = 1 ------>--}}
{{--                {!! Form::open(['method'=>'post','action'=>['Buyer\ShoppingCartController@status',$wishList->id]]) !!}--}}
{{--                @csrf--}}
{{--                {{Form::hidden('product_id',$wishList->product->id)}}--}}
{{--                {{Form::hidden('user_id',Auth::user()->id)}}--}}
{{--                {{Form::hidden('status',1)}}--}}
{{--                {{Form::hidden('priceTotal',0)}}--}}
{{--                {{Form::hidden('tax',2)}}--}}
{{--                {{Form::hidden('price',$wishList->product->price)}}--}}
{{--                {{Form::selectRange('quantity', 1, 50, ['placeholder'=> $wishList->quantity])}}--}}
{{--                {{Form::submit('Save',['class'=>'btn btn-success btn-info btn-sm'])}}--}}

{{--                {!! Form::close() !!}--}}
{{--                <!------End Remove all products------>--}}
{{--                    --}}
                    <a class="btn btn-success text-center" href="{{route('shoppingCart.checkout')}}" role="button">Proceed to checkout</a>
                </div>
            </div>

        </div>
    </div>









@endsection
