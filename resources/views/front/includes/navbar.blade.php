
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{route('public.index')}}" style="color:white;font-weight: bold;">PizzaShop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('public.index')}}" style="color:white;font-weight: bold;">Home</a>
                </li>
                @if(Auth::check())
                @if(Auth::user()->role == 1)
                    <a class="nav-link" style="color:mediumseagreen;font-weight: bolder;" href="{{route('admin.dashboard')}}"> Dashboard</a>
                @endif
                    @endif
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <!---  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> ---->

            </form>

            @if(Auth::check())

                @if(Auth::user()->role == 0)
                <a class="nav-link" style="color:mediumseagreen;font-weight: bolder;margin-top:-5px;" href="{{route('shoppingCart.index')}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    ShoppingCard @if(Auth::check()) ({{$productsZero}}) @endif</a>
                    @endif
                <ul class="nav" style="margin-top: 10px;">
                        <li class="dropdown">
                            <a href="#"  data-toggle="dropdown" style="color:lightseagreen">
                                <p style="font-weight: bolder;color:mediumseagreen;text-decoration: none;"><i class="fa fa-user"></i> {{Auth::user()->name}}</p>
                            </a>
                            <ul class="dropdown-menu bg-dark" >
                                <li><a href="{{route('logout')}}" style="color:white;font-weight: bolder;">Logout</a></li>

                                @if(Auth::user()->role ==0)
                                    <hr>
                                <li><a href="{{route('buyer.index')}}" style="color:white;font-weight: bolder;">Profile</a></li>
                                    @endif
                            </ul>
                        </li>
                    </ul>

            @else
                <a class="nav-link" style="color:greenyellow;font-weight: bold;" href="{{route('login')}}">Login</a>
                <a class="nav-link" style="color:greenyellow;font-weight: bold;" href="{{route('register')}}">Register</a>
            @endif
        </div>

    </div>
</nav>
                                              
