@include('front.includes.header')

<body style="background-color:slategrey;margin:0px;padding:0px;">

@include('front.includes.navbar')

<div class="container">
      @yield('content')
</div>

@include('front.includes.footer')
@include('front.includes.scripts')

<!---Gallery----->
<script src="{{asset('assets/js/scripts.js')}}"></script>
<!---End Gallery--->
</body>
</html>
