@extends('front.layouts.master')
@section('title') Pizza Shop @endsection
@section('content')
    <div  style="background-color:white;padding:3%;border-style: outset;border-width: 15px;">

        <button type="button" class="btn btn-danger btn-lg" style="margin-left: 40%;margin-right: 15%;">ORDER ONLINE NOW</button>
        <a href="#" class="fa fa-facebook"></a>
        <a href="#" class="fa fa-twitter"></a>
        <a href="#" class="fa fa-linkedin"></a>
        <a href="#" class="fa fa-pinterest"></a>


        @include('buyer.includes.messages')
    <div class="row">
        <div class="col-md-6 mt-5">
            <h3 class="text-center">Who we are?</h3>
            <p style="font-size: 17px;font-family: Verdana;text-align: justify;">PizzaShop is the first in the city in the number of orders ordered online.
                We have a rich assortment of pizzas.  Below you can see all the pizzas we have in our menu and order
                online. The speed of delivery depends on the location where you are, but thanks to our fast delivery
                we always try to get your pizza to arrive as soon as possible. We look forward to your order online.</p>
        </div>
        <div class="col-md-6 mt-5" style="background-image:url('/photos/PizzaShop.jpg');background-repeat: no-repeat;height:350px;width:100%;">


        </div>
    </div>
        <div class="row">
            <div class="col-md-6 mt-5" style="background-image:url('/photos/restoran2.jpg');background-repeat: no-repeat;height:350px;width:100%;">


            </div>
            <div class="col-md-6 mt-5">
                <h3 class="text-center">Private Celebrations</h3>
                <p style="font-size: 17px;font-family: Verdana;text-align: justify;">For specific requests, orders or large celebrations, contact our Manager  to directly call the
                    restaurant. PizzaShop restaurant is a newly opened restaurant found in Vracar,
                    Belgrade municipality. The offer of the restaurant includes private celebrations for every occasion.
                    Our offer of set menus is extremely affordable, but yet of a high quality. Besides set menus
                    included in our offer, it is up to you to create your own! Nota is here to make your wishes come
                    true.</p>
            </div>

        </div>


        <div class="row">
        <div class="col-md-12 mt-5" >
            <h1 class="text-center">---------------Menu-----------------</h1>
            <div class="row text-center">
                @foreach($products as $product)
                    <div class="col-lg-3 col-md-6 mb-4 mt-5">
                        <h6 class="card-title" style="font-weight: bolder;color:red;background-color: black;padding:2%;">{{$product->name}}</h6>
                        <div class="card" style="background-color: lightseagreen;border:none;">
                            <img class="card-img-top img-responsive" src="{{asset('photos/'.$product->image)}}" alt="{{asset('photos/'.$product->name)}}" style="height:200px;">
                        </div>
                        <div class="card-body mt-2" style="background-color: black;color:white;">
                        @if(Auth::check() && Auth::user()->role == 0)


                            <!--Adding item to ShoppingCart-->
                                {!! Form::open(['method'=>'post','action'=>'Buyer\ShoppingCartController@store']) !!}
                                @csrf

                                {{Form::hidden('product_id',$product->id)}}
                                {{Form::hidden('user_id', Auth::user()->id)}}
                                {{Form::hidden('price_sum',$product->price)}}
                                {{Form::hidden('status',0)}}


                                <strong>${{$product->price}}</strong>
                                {{Form::submit('Add To Chart',['class'=>'btn btn-success btn-info'])}}
                                {!! Form::close() !!}
                            <!--End Adding item to ShoppingCart-->


                            @else

                             <strong> ${{$product->price}}</strong>   <button type="button" class="btn btn-info"  style="background-color: lightskyblue;" onclick='return confirm("You must be logged in to proceed")' > Add To Cart</button>
                            @endif


                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
        <div class="row" style="background-color: #a6e1ec;">
            <h1  class="bg-dark text-center container-fluid" style="color:white;">Still can't decide what to eat? Check this out</h1>
            <!----SlidShow Gallery---->

            <div class="slideshow-container" style="border-color: white;border-style: outset;border-width: 15px; margin-top: -7px; ">
                <!-- Full-width images with number and caption text -->
                @foreach($products as $product)
                    <div class="mySlides fade">
                        <div class="numbertext"></div>
                        <img src="{{asset('photos/'.$product->image)}}" style="width:100%;height:500px;">
                        <div class="text">Caption Text</div>
                    </div>
            @endforeach
            <!-- Next and previous buttons -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
            </div>
            <br>
            <!-- The dots/circles -->
            <div style="text-align:center">
                <span class="dot" onclick="currentSlide(1)"></span>
                <span class="dot" onclick="currentSlide(2)"></span>
                <span class="dot" onclick="currentSlide(3)"></span>
            </div>

            <!----End Gallery---->


        </div>

<div class="row" style="margin-left: -3.5%;margin-right: -3.5%;">
    <div class="col-lg-12">

    </div>
</div>
<div class="row" style="background-color: lightslategrey;">
    <div class="col-md-6 mt-5" >
        <h1  class=" text-center" style="color:white;margin-top:30%;font-size:42px;">We Are Open 24/7</h1>
        <h1 class=" text-center">Call Us </h1>
    </div>
    <div class="col-md-6" style="background-image:url('/photos/bakery.png');background-repeat: no-repeat;height:600px;width:100%;">

    </div>
</div>
<br>

        <div class="row" style="background-color:lightblue;height:300px;">
            <div class="col-md-6 mt-5">
                <h3>How you can contact us?</h3>
                <p style="font-size: 17px;font-family: Verdana;text-align: justify;">You can contact us directly
                    through our manager. If you have any questions regarding the order, type of delivery or simply want to
                    leave us advice on how to improve your enjoyment feel free to contact us by phone or send us a
                    message</p>
            </div>
            <div class="col-md-6 mt-5">
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{asset('photos/profile.png')}}" style="width:200px;height:200px;border-radius: 100px;margin-left:20px;">
                    </div>
                    <div class="col-md-6">
                        <h3>Our Menager</h3>
                        <p>Name: Tea</p>
                        <p>Last name: Kljajin</p>
                        <p>E-mail: teakljajin@gmail.com</p>
                        <p>Contact: 064/2577709</p>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row" style="background-image: url('photos/coffee.jpg')">
            <div class="col-md-12" style="padding:5%;">
                {{Form::open(['method'=>'post','action'=>'Admin\MessageController@store'])}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" style="font-weight: bolder;color:white;">
                            {{Form::label('name','Name:')}}
                            {{Form::text('name','',['class'=>'form-control border-input','placeholder'=>'name'])}}
                            @error('name')<p style="color:red;">{{$message}}</p> @enderror
                        </div>
                        <div class="form-group" style="font-weight: bolder;color:white;">
                            {{Form::label('email','Email:')}}
                            {{Form::text('email','',['class'=>'form-control border-input','placeholder'=>'email'])}}
                            @error('email')<p style="color:red;">{{$message}}</p> @enderror
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="form-group" style="font-weight: bolder;">
                            {{Form::label('message','Message:')}}
                            {{Form::textArea('message','',['class'=>'form-control border-input','placeholder'=>'Type your message hear','rows'=>5])}}
                            @error('message')<p style="color:red;">{{$message}}</p> @enderror
                        </div>
                        <div class="form-group">
                            {{Form::submit('Submit',['class'=>'btn btn-primary btn-sm pull-right'])}}
                        </div>

                    </div>

                </div>
                {{Form::close()}}
            </div>
        </div>




    </div>
@endsection
