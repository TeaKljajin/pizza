@extends('front.layouts.master')
@section('title') Login  @endsection
@section('content')

    @if(Auth::check())
        <script>window.location='/'</script>
    @endif

    <h3 class="text-center mt-5">Sign in</h3>

    <div class="row" style="margin-bottom: 40%;">
        <div class="col-md-3">

        </div>
        <div class="col-md-6 mt-5">
            @if(session()->has('errorMessage'))
                <div class="alert alert-danger">
                    {{session()->get('errorMessage')}}
                </div>
            @endIf
            {!! Form::open(['method'=>'post','action'=>'Front\LoginController@store']) !!}
            @csrf
            {{Form::hidden('role',0)}}

            <div class="form-group">
                {{Form::label('email','Email:')}}
                {{Form::email('email','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'email','placeholder'=>'Email'])}}
                @error('email') <div class="alert alert-danger">{{$message}}</div> @enderror
            </div>
            <div class="form-group">
                {{Form::label('password','Password:')}}
                {{Form::password('password',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'password','placeholder'=>'password'])}}
                @error('password')<div class="alert alert-danger">{{$message}}</div> @enderror
            </div>
            <div class="form-group">
                {{Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])}}
            </div>

            {!! Form::close() !!}


        </div>
        <div class="col-md-3">

        </div>
    </div>
@endsection
