@extends('front.layouts.master')
@section('title') Register @endsection
@section('content')
    @if(Auth::check())
        <script>window.location='/'</script>
    @endif

    <h3 class="text-center mt-5">Sign Up</h3>
    <div class="row" style="margin-bottom: 15%;">
        <div class="col-md-3">

        </div>
        <div class="col-md-6 mt-5">
            {!! Form::open(['method'=>'post','action'=>'Admin\UsersController@store']) !!}
            @csrf
            {{Form::hidden('role',0)}}

                <div class="form-group">
                    {{Form::label('name','Name:')}}
                    {{Form::text('name','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'name','placeholder'=>'Name'])}}
                    @error('name') <div class="alert alert-danger">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    {{Form::label('lastName','Last Name:')}}
                    {{Form::text('lastName','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'lastName','placeholder'=>'Last name'])}}
                    @error('lastName') <div class="alert alert-danger">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    {{Form::label('email','Email:')}}
                    {{Form::email('email','',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'email','placeholder'=>'Email'])}}
                    @error('email') <div class="alert alert-danger">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    {{Form::label('password','Password:')}}
                    {{Form::password('password',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'password','placeholder'=>'password'])}}
                    @error('password')<div class="alert alert-danger">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    {{Form::label('password_confirmation','Confirm Password:')}}
                    {{Form::password('password_confirmation',['class'=>'form-control border-input','autocomplete'=>'off','id'=>'password_confirmation','placeholder'=>'confirm password'])}}
                    @error('password_confirmation')<div class="alert alert-danger">{{$message}}</div> @enderror
                </div>
            <div class="form-group">
                {{Form::submit('Submit',['class'=>'btn btn-primary btn-sm'])}}
            </div>

            {!! Form::close() !!}


        </div>
        <div class="col-md-3">

        </div>
    </div>
@endsection



