<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Front-end
Route::get('/','Front\PublicController@index')->name('public.index');
Route::get('/product/details/{id}','Front\PublicController@product')->name('public.product.details');

//Login
Route::get('/login','Front\LoginController@login')->name('login');
Route::post('/login/store','Front\LoginController@store')->name('login.store');

//Register
Route::get('/register','Front\LoginController@register')->name('register');
Route::post('/register/store','Admin\UsersController@store')->name('register.store');

//Logout
Route::get('/logout','Front\LoginController@logout')->name('logout');



//Admin
Route::group(['prefix'=>'admin','middleware'=>['auth'=>'admin']],function() {
    //Admin Dashboard
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
    //Products
    Route::resource('/products', 'Admin\ProductsController');
    //Orders
    Route::get('/orders', 'Admin\OrdersController@index')->name('orders.index');
    Route::get('/orders/show/{id}', 'Admin\OrdersController@show')->name('orders.show');
    Route::get('/orders/pending/{id}', 'Admin\OrdersController@pending')->name('order.pending');
    Route::get('/orders/confirm/{id}', 'Admin\OrdersController@confirm')->name('order.confirm');
    //Users
    Route::get('/users', 'Admin\UsersController@index')->name('users.index');
    Route::get('/users/show/{id}', 'Admin\UsersController@show')->name('users.show');
    Route::post('/users/store', 'Admin\UsersController@store')->name('users.store');
    Route::post('/users/update', 'Admin\UsersController@update')->name('users.update');

    //Message
    Route::get('/message/', 'Admin\MessageController@index')->name('message.index');
    Route::post('/message/store', 'Admin\MessageController@store')->name('message.store');
    Route::post('/message/delete/{id}', 'Admin\MessageController@destroy')->name('message.destroy');



});



//Buyer
Route::group(['prefix'=>'buyer','middleware'=>['auth'=>'buyer']],function() {

    //Buyer profile page
    Route::get('/profile','Buyer\ProfileController@index')->name('buyer.index');
    Route::get('/profile/edit/{id}','Buyer\ProfileController@edit')->name('buyer.edit');
    Route::post('/profile/update/{id}','Admin\UsersController@update')->name('buyer.update');
    Route::get('/profile/show/{id}','Buyer\ProfileController@show')->name('buyer.show');
    Route::get('/profile/details/{id}','Buyer\ProfileController@details')->name('buyer.details');


    //ShoppingCart
    Route::get('/shoppingCart', 'Buyer\ShoppingCartController@index')->name('shoppingCart.index');
    Route::post('/shoppingCart/store', 'Buyer\ShoppingCartController@store')->name('shoppingCart.store');
    Route::post('/shoppingCart/quantity/{id}', 'Buyer\ShoppingCartController@quantity')->name('shoppingCart.quantity');
    Route::post('/shoppingCart/destroy/{id}', 'Buyer\ShoppingCartController@destroy')->name('shoppingCart.destroy');
    //Checkout
    Route::get('/checkout', 'Buyer\ShoppingCartController@proceed')->name('shoppingCart.checkout');
    //Store Delivery address
    Route::post('/checkout/store', 'Admin\OrdersController@store')->name('shoppingCart.store');
    //Store Order + Products in pivot table
    Route::post('checkout/pay', 'Admin\OrdersController@pay')->name('order.pay');



});
